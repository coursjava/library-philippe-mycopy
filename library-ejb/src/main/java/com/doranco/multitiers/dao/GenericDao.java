package com.doranco.multitiers.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GenericDao<T> {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("library");
	EntityManager em = emf.createEntityManager();
	public EntityTransaction transaction = em.getTransaction();

	public T create(T toBeCreated) {

		transaction.begin();
		em.persist(toBeCreated);
		transaction.commit();
		return toBeCreated;

	}

	public void delete(T toBeDeleted) {
		transaction.begin();
		em.remove(toBeDeleted);
		transaction.commit();
	}

	public T find(Class clazz, Integer primaryKey) {		
		return (T) em.find(clazz, primaryKey);
	}
	
//	public List<T> findAll(Class clazz){
//		
//	}

	public T update(T toBeUpdated) {

		transaction.begin();
		em.merge(toBeUpdated);
		transaction.commit();
		return toBeUpdated;
	}

}
