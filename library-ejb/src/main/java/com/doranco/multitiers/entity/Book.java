package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "l_book")
public class Book extends Identifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4484249362256718453L;

	private String ISBN;
	private String title;

	@OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
	private Set<Note> notes;

	@OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
	private Set<Viewing> viewings;

	public Book() {

	}

	public Set<Note> getNotes() {
		return notes;
	}

	public void setNotes(Set<Note> notes) {
		this.notes = notes;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
